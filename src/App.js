import React, { Component } from 'react';
import {Route, Switch} from "react-router-dom";

import Us from './components/Us/Us.js';
import Portfolio from './components/Portfolio/Portfolio';
import Prices from './components/Prices/Prices';


class App extends Component {
  render() {
    return (
        <Switch>
          <Route path="/" exact component={Us} />
          <Route path="/portfolio" component={Portfolio} />
          <Route path="/prices" component={Prices} />
        </Switch>
    );
  }
}

export default App;
