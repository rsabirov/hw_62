import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';
import './header.css';

class Header extends Component {
  render() {
    return (
        <header className="header">
          <div className="container">
            <div className="row">
              <div className="col-md-2">
                <div className="logo-box">
                  <a href="#" className="logo-box__logo">Uno</a>
                </div>
              </div>
              <div className="col-md-4">
                <div className="menu-box">
                  <nav className="site-menu">
                    <ul className="site-menu__list">
                      <li className="site-menu__item">
                        <NavLink to="/">Мы</NavLink>
                      </li>
                      <li className="site-menu__item">
                        <NavLink to="/portfolio">Портфолио</NavLink>
                      </li>
                      <li className="site-menu__item">
                        <NavLink to="/prices">Ценовые решения</NavLink>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
              <div className="col-md-6">
                <div className="info-box">
                   <div className="row">
                     <div className="col-md-6">
                       <span className="info-box__phone">0550 96 55 00</span>
                     </div>
                     <div className="col-md-6">
                        <div className="soc-nets">
                          <a href="#" className="soc-nets-link soc-nets-link--insta" />
                          <a href="#" className="soc-nets-link soc-nets-link--fb" />
                        </div>
                     </div>
                   </div>
                </div>
              </div>
            </div>
          </div>
        </header>
    );
  }
}

export default Header;