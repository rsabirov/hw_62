import React, {Fragment} from 'react';
import Header from '../../shared/Header/Header';
import portfolioImg1 from '../../assets/img/portfolio-img-1.jpg';
import portfolioImg2 from '../../assets/img/portfolio-img-2.jpg';
import portfolioImg3 from '../../assets/img/portfolio-img-3.jpg';
import portfolioImg4 from '../../assets/img/portfolio-img-4.jpg';

const Portfolio = props => {
    return (
        <Fragment>
            <Header />
            <section className="portfolio">
                <div className="row justify-content-md-center">
                    <div className="col-md-5 col-xl-5">
                        <div className="sect-header">
                            <h1 className="sect-header__title">Портфолио</h1>
                            <p className="sect-header__text">Здесь вы сможете познакомиться с нашими работами и сформировать
                                свое представление о том, каким должен быть Ваш идеальный дизайн интерьера! </p>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row no-gutters">
                        <div className="col-md-3"><img className="img-fluid" src={portfolioImg1} alt=""/></div>
                        <div className="col-md-3"><img className="img-fluid" src={portfolioImg2} alt=""/></div>
                        <div className="col-md-3"><img className="img-fluid" src={portfolioImg3} alt=""/></div>
                        <div className="col-md-3"><img className="img-fluid" src={portfolioImg4} alt=""/></div>
                        <div className="col-md-3"><img className="img-fluid" src={portfolioImg1} alt=""/></div>
                        <div className="col-md-3"><img className="img-fluid" src={portfolioImg2} alt=""/></div>
                        <div className="col-md-3"><img className="img-fluid" src={portfolioImg3} alt=""/></div>
                        <div className="col-md-3"><img className="img-fluid" src={portfolioImg4} alt=""/></div>
                    </div>
                </div>
                <div className="more-projects">
                    <div className="container">
                        <button className="btn">Больше проектов</button>
                    </div>
                </div>
            </section>
        </Fragment>
    )
};

export default Portfolio;