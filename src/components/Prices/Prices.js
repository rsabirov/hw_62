import React, {Fragment} from 'react';
import Header from '../../shared/Header/Header';

const Prices = props => {
    return (
        <Fragment>
            <Header />
            <section className="prices">
                <div className="container">
                <div className="row justify-content-md-center">
                    <div className="col-md-4">
                        <div className="sect-header">
                            <h1 className="sect-header__title sect-header__title--prices">Ценовые решения</h1>
                        </div>
                    </div>
                </div>
                </div>
                <div className="container">
                    <div className="row no-gutters price-amounts">
                        <div className="col-md-4 text-left">
                            <h3 className="price-amounts__title">UNO</h3>
                            <span className="price-amounts__text">7 USD</span>
                        </div>
                        <div className="col-md-4 text-left">
                            <h3 className="price-amounts__title">UNO</h3>
                            <span className="price-amounts__text">15 USD</span>
                        </div>
                        <div className="col-md-4 text-left">
                            <h3 className="price-amounts__title">UNO</h3>
                            <span className="price-amounts__text">PLUS</span>
                        </div>
                    </div>
                    <div className="row no-gutters price-amounts">
                        <div className="col-md-4 text-left">
                            <h3 className="price-amounts__title">UNO</h3>
                            <span className="price-amounts__text">7 USD</span>
                        </div>
                        <div className="col-md-4 text-left">
                            <h3 className="price-amounts__title">UNO</h3>
                            <span className="price-amounts__text">15 USD</span>
                        </div>
                        <div className="col-md-4 text-left">
                            <h3 className="price-amounts__title">UNO</h3>
                            <span className="price-amounts__text">PLUS</span>
                        </div>
                    </div>
                </div>
                <div className="download-price-list">
                    <div className="container">
                        <div className="text-img" />
                        <button className="btn btn--white">Скачать</button>
                    </div>
                </div>
            </section>
        </Fragment>
    )
};

export default Prices;