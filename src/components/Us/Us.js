import React, {Fragment} from 'react';
import Header from '../../shared/Header/Header';

const Us = props => {
    return (
        <Fragment>
            <Header />
            <section className="jumbo">
                <div className="jumbotron jumbotron-fluid">
                    <div className="container">
                        <div className="jumbo__circle">
                            <div className="jumbo__circle-inner">
                                <h1 className="jumbo__circle-title">Uno design</h1>
                                <p className="jumbo__circle-text">Безупречный дизайн интерьера</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="us">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <h2 className="us__slogan">
                                Позвольте представиться,
                                мы – студия UNO design!
                            </h2>
                        </div>
                        <div className="col-md-6">
                            <p className="us__text">
                                Наша специализация – профессиональное проектирование
                                интерьеров любой сложности.
                                Проекты студии реализованы во многих столичных кофейнях,
                                ресторанах, магазинах, частных домах и квартирах.</p>
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    )
};

export default Us;